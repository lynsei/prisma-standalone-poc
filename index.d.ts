type LogLevel = 'info' | 'query' | 'warn';
type LogDefinition = {
  level: LogLevel;
  emit: 'stdout' | 'event';
};