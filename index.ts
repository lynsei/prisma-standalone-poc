//import { PrismaClient } from '@prisma/client'
require('dotenv').config()

async function main() {
  const { PrismaClient } = require('@prisma/client')
  const prisma = new PrismaClient({
    datasources: {
      db: {
        url: process.env.DATABASE_URL,
      },
    },
    log: [
      {
        emit: 'stdout',
        level: 'query',
      },
      {
        emit: 'stdout',
        level: 'info',
      },
      {
        emit: 'stdout',
        level: 'warn',
      },
    ]
  })

  const users = await prisma.users.findAll()
  users.on('query', console.log(this))
}

